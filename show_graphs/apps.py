from django.apps import AppConfig


class ShowGraphsConfig(AppConfig):
    name = 'show_graphs'
