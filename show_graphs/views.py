from django.shortcuts import render,render_to_response
import psutil
from yahoo_finance import Share
import datetime
from random import randint
from pymongo import MongoClient
import csv
from collections import Counter
from operator import itemgetter
import pandas as p





def show_graphs4(request):
    cpu_percent = psutil.cpu_percent()
    memory =  psutil.virtual_memory()
    memory = memory.percent
    return  render(request ,'show_graphs/new.html',{"cpu_percent":cpu_percent,"memory":memory})


def show_graphs3(request):

    now = datetime.datetime.now()
    date_to = str(now.date())
    BAC = Share('BAC')
    MET = Share('MET')
    zzz = BAC.get_historical('2016-06-20', date_to)
    zzz.reverse()
    zzz1 = MET.get_historical('2016-06-20', date_to)
    zzz1.reverse()
    new_list = [['date','METLIFE','Bank of America']]
    for i in range(len(zzz)):
        list_ = []
        list_ = [zzz[i]['Date'],int(zzz[i]['Volume']),int(zzz1[i]['Volume'])]
        new_list.append(list_)

    return render(request,  'show_graphs/new1.html', {'djangodict': new_list,"date_to":date_to})


def show_graphs2(request):
    list_ = []
    for i in range(100):
        zzz = [i,randint(10,500)]
        list_.append(zzz)
    return render(request,  'show_graphs/new2.html',{'djangodict': list_})


def show_graphs(request):
   db = MongoClient()
   zzz = db.lfc

   with open('tweets.csv', 'w') as outfile:
         fieldnames = ['text', 'user', 'created_at', 'geo']
         writer = csv.DictWriter(outfile, delimiter=',', fieldnames=fieldnames)
         writer.writeheader()

         for data in zzz.kausik_railways.find():
             writer.writerow({
             'text': data['text'].encode('utf-8'),
             'user': data['user'].encode('utf-8'),
             'created_at': data['created_at'],
             'geo': data['geo']
             })
   outfile.close()
   tweets = p.read_csv('tweets.csv')
   #get_date = [i[11:16] for i in tweets['created_at']
   #count = [[i[0],i[1]] for i in sorted(dict(Counter(get_date)).items(), key=itemgetter(0))]
   get_time_date = [str(datetime.datetime.strptime(i,'%Y-%m-%d %H:%M:%S').time())[:-3] for i in tweets['created_at']]
   count = [[i[0],i[1]] for i in sorted(dict(Counter(get_time_date)).items(), key=itemgetter(0))]
   count.insert(0,['time','count'])
   return render(request,  'show_graphs/new3.html',{'djangodict': count})

def final(request):
    return render_to_response('show_graphs/testpage.html')


