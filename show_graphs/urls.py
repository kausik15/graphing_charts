from django.conf.urls import url,include
from .views import show_graphs,show_graphs3,show_graphs4,show_graphs2,final
urlpatterns = [
    url(r'^one/$', show_graphs),
    url(r'^two/$', show_graphs2),
    url(r'^three/$', show_graphs3),
    url(r'^four/$', show_graphs4),
    url(r'^final/$', final),
]
